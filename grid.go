package maze

import (
	"bytes"
	"fmt"
	"math/rand"
	"strconv"
	"strings"
	"time"
)

// change to grid
type Grid interface {
	Rows() int
	Columns() int
	RandomCell() *Cell
	Size() int
	EachCell() chan *Cell
	EachRow() chan []*Cell
}

// change to simple grid
type SimpleGrid struct {
	rows, columns int
	grid          [][]*Cell
}

func NewGrid(rows int, columns int) *SimpleGrid {
	rand.Seed(time.Now().UTC().UnixNano())

	// prepare grid
	cells := make([][]*Cell, rows)

	for r := 0; r < rows; r++ {
		cols := make([]*Cell, columns)
		for c := 0; c < columns; c++ {
			cols[c] = newCell(r, c)
		}
		cells[r] = cols
	}

	// create Grid structure
	g := &SimpleGrid{rows, columns, cells}

	// configure grid
	for cell := range g.EachCell() {
		r := cell.row
		c := cell.column
		cell.north = g.Cell(r-1, c)
		cell.south = g.Cell(r+1, c)
		cell.west = g.Cell(r, c-1)
		cell.east = g.Cell(r, c+1)
	}

	return g
}

func (g *SimpleGrid) EachRow() chan []*Cell {
	yield := make(chan []*Cell)
	go func() {
		for r := 0; r < g.rows; r++ {
			yield <- g.grid[r]
		}

		close(yield)
	}()

	return yield
}

//  EachCell returns a channel that contains all cells in order.
// for cell := range g.EachCell() { ... foo(cell) ... }
func (g *SimpleGrid) EachCell() chan *Cell {
	yield := make(chan *Cell)
	go func() {
		for r := 0; r < g.rows; r++ {
			for c := 0; c < g.columns; c++ {
				yield <- g.grid[r][c]
			}
		}

		close(yield)
	}()

	return yield
}

// Return specified cell or row/column nil if out of bounds
func (g *SimpleGrid) Cell(row int, column int) *Cell {
	if row < 0 || row >= g.rows {
		return nil
	}

	if column < 0 || column >= g.columns {
		return nil
	}

	return g.grid[row][column]
}

func (g *SimpleGrid) RandomCell() *Cell {
	return g.Cell(rand.Intn(g.rows), rand.Intn(g.columns))
}

func (g *SimpleGrid) Rows() int {
	return g.rows
}

func (g *SimpleGrid) Columns() int {
	return g.columns
}

// Size returns the number of cells in the rectangular grid.
func (g *SimpleGrid) Size() int {
	return g.rows * g.columns
}

func (g *SimpleGrid) String() string {
	return g.ToString(func(c *Cell) string {
		return "   " // up to 3 chars wide
	})
}

func (g *SimpleGrid) ToString(content func(c *Cell) string) string {
	var out, top, bottom bytes.Buffer
	out.WriteString("+")
	out.WriteString(strings.Repeat("---+", g.columns))
	out.WriteString("\n")

	for r := 0; r < g.rows; r++ {
		top.WriteString("|")
		bottom.WriteString("+")

		for c := 0; c < g.columns; c++ {

			cell := g.Cell(r, c)
			if cell == nil {
				top.WriteString("XXX|")
				bottom.WriteString("---+")
			} else {
				contents := content(cell)

				if cell.IsLinked(cell.east) {
					top.WriteString(fmt.Sprintf("%3s ", contents))
				} else {
					top.WriteString(fmt.Sprintf("%3s|", contents))
				}

				if cell.IsLinked(cell.south) {
					bottom.WriteString("   +")
				} else {
					bottom.WriteString("---+")
				}
			}
		}

		out.WriteString(top.String())
		out.WriteString("\n")
		out.WriteString(bottom.String())
		out.WriteString("\n")

		top.Reset()
		bottom.Reset()
	}

	return out.String()
}

// Output hex with bitmask for walls.
//   1 : North
//   2 : South
//   4 : East
//   8 : West
func (g *SimpleGrid) ToHex() string {
	var out bytes.Buffer

	for cell := range g.EachCell() {
		out.WriteString(strconv.FormatInt(int64(cell.WallMask()), 16))
	}

	return out.String()
}

// sample returns a random cell from the given slice
func sample(run []*Cell) *Cell {
	cnt := len(run)

	if cnt > 0 {
		return run[rand.Intn(cnt)]
	} else {
		return nil
	}
}

// indexOf returns the position of the cell in array
func indexOf(cells []*Cell, cell *Cell) int {
	hash := cell.String()

	for i := 0; i < len(cells); i++ {
		if cells[i].String() == hash {
			return i
		}
	}

	return -1
}

// deleteCell deletes the specified cell and returns a new slice
func deleteCell(cells []*Cell, cell *Cell) []*Cell {
	index := indexOf(cells, cell)

	if index >= 0 {
		cells = append(cells[:index], cells[index+1:]...)
	}

	return cells
}

func unlinkedCells(cells []*Cell) []*Cell {
	var unlinked []*Cell

	for _, c := range cells {
		if len(c.Links()) == 0 {
			unlinked = append(unlinked, c)
		}
	}

	return unlinked
}

func linkedCells(cells []*Cell) []*Cell {
	var unlinked []*Cell

	for _, c := range cells {
		if len(c.Links()) > 0 {
			unlinked = append(unlinked, c)
		}
	}

	return unlinked
}
