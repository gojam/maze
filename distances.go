package maze

type distanceval struct {
	cell     *Cell
	distance int
}

type distances struct {
	root  *Cell
	cells map[string]distanceval
}

// Create new Distances struct
func newDistances(root *Cell) *distances {
	cells := make(map[string]distanceval)
	cells[root.String()] = distanceval{root, 0}
	return &distances{root: root, cells: cells}
}

// Distance returns distance between specified cell and root.
func (d *distances) Distance(cell *Cell) int {
	if val, ok := d.cells[cell.String()]; ok {
		return val.distance
	} else {
		return -1
	}
}

// setDistance sets the distance bewtween the specified cell and root.
func (d *distances) setDistance(cell *Cell, distance int) {
	d.cells[cell.String()] = distanceval{cell, distance}
}

// Cells returns all the cells in the distance grid.  Note that the cells are
// returned in an indeterminate order.
func (d *distances) Cells() []*Cell {
	var list = make([]*Cell, len(d.cells))
	i := 0
	for _, v := range d.cells {
		list[i] = v.cell
		i++
	}

	return list
}

// PathTo finds the shortest path between the specified cell and root.
func (d *distances) PathTo(goal *Cell) *distances {
	current := goal

	breadcrumbs := newDistances(d.root)
	breadcrumbs.setDistance(current, d.Distance(current))
	for current.String() != d.root.String() {
		for _, neighbor := range current.Links() {
			if d.Distance(neighbor) < d.Distance(current) {
				breadcrumbs.setDistance(neighbor, d.Distance(neighbor))
				current = neighbor
			}
		}

	}
	return breadcrumbs
}

// Max finds the cell furthest from root, returning the distance measurement as well.
func (d *distances) Max() (maxCell *Cell, maxDistance int) {
	maxDistance = 0
	maxCell = d.root

	for _, dval := range d.cells {
		if dval.distance > maxDistance {
			maxCell = dval.cell
			maxDistance = dval.distance
		}
	}

	return
}

// LongestPath returns the two points farthest away from each other in the maze.
func (d *distances) LongestPath() *distances {
	newStartCell, _ := d.Max()
	newDists := newDistances(newStartCell)
	goalCell, _ := newDists.Max()

	return d.PathTo(goalCell)
}
