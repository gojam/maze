package maze

func Wilsons(g Grid) {
	var unvisited []*Cell

	for c := range g.EachCell() {
		unvisited = append(unvisited, c)
	}

	first := sample(unvisited)
	unvisited = deleteCell(unvisited, first)

	for len(unvisited) > 0 {
		cell := sample(unvisited)

		var path []*Cell
		path = append(path, cell)

		for indexOf(unvisited, cell) >= 0 {
			cell = cell.RandomNeighbor()
			position := indexOf(path, cell)
			if position >= 0 {
				// prevent loop
				path = path[0 : position+1]
			} else {
				path = append(path, cell)
			}
		}

		for i := 0; i < len(path)-1; i++ {
			path[i].Link(path[i+1], true)
			unvisited = deleteCell(unvisited, path[i])
		}
	}
}
