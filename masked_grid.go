package maze

import (
	"math/rand"
	"time"
)

type MaskedGrid struct {
	mask *Mask
	*SimpleGrid
}

func NewMaskedGrid(mask *Mask) *MaskedGrid {
	rand.Seed(time.Now().UTC().UnixNano())

	// Order of operations is important here

	// prepare grid by creating a cell for every enabled bit in the mask
	cells := make([][]*Cell, mask.rows)

	for r := 0; r < mask.rows; r++ {
		list := make([]*Cell, mask.columns)
		for c := 0; c < mask.columns; c++ {
			if mask.Get(r, c) {
				list[c] = newCell(r, c)
			}
		}
		cells[r] = list
	}

	// create underlying data structure
	mg := &MaskedGrid{mask, &SimpleGrid{mask.rows, mask.columns, cells}}

	// now that underlying Grid is created, EachCell works
	for cell := range mg.EachCell() {
		r := cell.row
		c := cell.column
		cell.north = mg.Cell(r-1, c)
		cell.south = mg.Cell(r+1, c)
		cell.west = mg.Cell(r, c-1)
		cell.east = mg.Cell(r, c+1)
	}

	return mg
}

func (mg *MaskedGrid) Rows() int {
	return mg.mask.rows
}

func (mg *MaskedGrid) Columns() int {
	return mg.mask.columns
}

// EachCell returns a channel that returns every cell in order.
func (mg *MaskedGrid) EachCell() chan *Cell {
	yield := make(chan *Cell)
	go func() {
		for r := 0; r < mg.rows; r++ {
			for c := 0; c < mg.columns; c++ {
				if mg.mask.Get(r, c) {
					yield <- mg.grid[r][c]
				}
			}
		}

		close(yield)
	}()

	return yield
}

func (g *MaskedGrid) EachRow() chan []*Cell {
	yield := make(chan []*Cell)
	go func() {
		for r := 0; r < g.rows; r++ {
			yield <- g.grid[r]
		}

		close(yield)
	}()

	return yield
}

func (mg *MaskedGrid) RandomCell() *Cell {
	row, col := mg.mask.RandomLocation()
	return mg.Cell(row, col)
}

func (mg *MaskedGrid) Size() int {
	return mg.mask.Count()
}
