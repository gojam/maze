module gitlab.com/gojam/maze

go 1.12

require (
	github.com/fogleman/gg v1.3.0
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0 // indirect
	golang.org/x/image v0.0.0-20190829093649-6ea169446634 // indirect
)
