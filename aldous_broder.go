package maze

func AldousBroder(g Grid) {
	cell := g.RandomCell()
	unvisited := g.Size() - 1

	for unvisited > 0 {
		neighbor := cell.RandomNeighbor()
		if len(neighbor.Links()) == 0 {
			cell.Link(neighbor, true)
			unvisited -= 1
		}

		cell = neighbor
	}
}
