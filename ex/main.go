package main

import (
	"fmt"

	"image/color"

	"gitlab.com/gojam/maze"
)

func main() {
	g := maze.NewGrid(20, 20)
	maze.ToPolarPNG("./polar.png", g, maze.NewOptions(maze.CellSize(8), maze.LineColor(color.White)))

	aldousBorder()
	binaryTree()
	huntAndKill()
	recursiveBacktracker()
	sidewinder()
	wilsons() // SLOW!
}

func aldousBorder() {
	g := maze.NewGrid(20, 20)
	maze.AldousBroder(g)
	maze.SolutionPNG(fmt.Sprintf("./aldous.png"), g)

	g = maze.NewGrid(20, 20)
	maze.AldousBroder(g)
	maze.PlainPNG("./aldous-plain.png", g)
}

func binaryTree() {
	g := maze.NewGrid(12, 12)
	maze.BinaryTree(g)
	fmt.Println(g)
	fmt.Println(g.ToHex())
}

func huntAndKill() {
	g := maze.NewGrid(20, 20)
	maze.HuntAndKill(g)
	maze.ColorDistancePNG("./hunt-and-kill.png", g)
}

func recursiveBacktracker() {
	g := maze.NewGrid(20, 20)
	maze.RecursiveBacktracker(g)
	maze.ColorDistancePNG("./recursiveBacktracker.png", g)
}

func sidewinder() {
	g := maze.NewGrid(20, 20)
	maze.Sidewinder(g)
	maze.ColorDistancePNG("./sidewinder.png", g)
}

func wilsons() {
	g := maze.NewGrid(10, 10)
	maze.Wilsons(g)
	maze.ColorDistancePNG("./wilsons.png", g)
}
