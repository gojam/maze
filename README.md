# Mazes in GO

Learning GO by learning mazes (circa 2015).  Modernized it with Go Modules (2019).  Maybe I should implment the rest of the book?

This code is based on [Mazes for Programmers](https://pragprog.com/book/jbmaze/mazes-for-programmers) by Jamis Buck.  The book implements the algortithms in Ruby, which is ironic because profressionally, I'm now transitioning all these years later from Go to Ruby.

## Draw 2D

* [Documentation](https://github.com/llgcode/draw2d/)

