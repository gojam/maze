package maze

func RecursiveBacktracker(g Grid) {
	startAt := g.RandomCell()
	stack := NewStack()
	stack.Push(startAt)

	for !stack.Empty() {
		current := stack.Peek().(*Cell)
		neighbors := unlinkedCells(current.Neighbors())

		if len(neighbors) == 0 {
			stack.Pop()
		} else {
			neighbor := sample(neighbors)
			current.Link(neighbor, true)
			stack.Push(neighbor)
		}
	}
}
