package maze

func BinaryTree(g Grid) {
	for cell := range g.EachCell() {
		var neighbors []*Cell
		if cell.north != nil {
			neighbors = append(neighbors, cell.north)
		}

		if cell.east != nil {
			neighbors = append(neighbors, cell.east)
		}

		cell.Link(sample(neighbors), true)
	}
}
