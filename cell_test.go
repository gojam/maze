package maze

import (
	"testing"
)

func TestCellLinks(t *testing.T) {
	cell0 := newCell(0, 0)
	cell1 := newCell(0, 1)
	cell2 := newCell(0, 2)
	cell0.Link(cell1, true)
	cell1.Link(cell2, true)

	//
	// IsLinked
	//
	if !cell0.IsLinked(cell1) {
		t.Fatal("Cell0 is not linked to Cell1")
	}

	if !cell1.IsLinked(cell0) {
		t.Fatal("Cell1 is not linked to Cell0")
	}

	if !cell1.IsLinked(cell2) {
		t.Fatal("Cell1 is not linked to Cell2")
	}

	if !cell2.IsLinked(cell1) {
		t.Fatal("Cell2 is not linked to Cell1")
	}

	if cell0.IsLinked(cell2) {
		t.Fatal("Cell0 is linked to Cell2")
	}

	//
	// Links
	//
	if len(cell0.Links()) != 1 {
		t.Fatal("Cell0 does not have 1 link")
	}

	if len(cell1.Links()) != 2 {
		t.Fatal("Cell1 does not have 2 links")
	}

	//
	// Unlink
	//
	cell0.Unlink(cell1, true)
	if len(cell0.Links()) != 0 {
		t.Fatal("Cell0 could not unlink Cell1")
	}

	if len(cell1.Links()) != 1 {
		t.Fatal("Cell1 does not have 1 link after unlink operation")
	}

}
