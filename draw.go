package maze

import (
	"fmt"
	"image/color"
	"math"

	"github.com/fogleman/gg"
)

type ColorFunc func(c *Cell) color.Color
type OptionsFunc func(*Options)

type Options struct {
	CellSize  int
	CellColor ColorFunc
	LineWidth int
	LineColor color.Color
}

func NewOptions(options ...func(*Options)) *Options {
	opts := &Options{}
	opts.CellSize = 16
	opts.LineWidth = 1
	opts.LineColor = color.Black
	opts.CellColor = func(c *Cell) color.Color { return color.White }

	for _, op := range options {
		op(opts)
	}

	return opts
}

func LineColor(c color.Color) OptionsFunc { return func(img *Options) { img.LineColor = c } }
func LineWidth(w int) OptionsFunc         { return func(img *Options) { img.LineWidth = w } }
func CellSize(s int) OptionsFunc          { return func(img *Options) { img.CellSize = s } }
func CellColor(c ColorFunc) OptionsFunc   { return func(img *Options) { img.CellColor = c } }

func line(gc *gg.Context, drawIt bool, x1, y1, x2, y2 float64) {
	if drawIt {
		gc.DrawLine(x1, y1, x2, y2)
		gc.Stroke()
	}
}

func ToPolarPNG(
	filePath string,
	g Grid,
	opts *Options,
) {

	imgSize := 2 * (g.Rows() + opts.LineWidth) * opts.CellSize

	gc := gg.NewContext(imgSize, imgSize)
	//gc.SetColor(opts.Background) // need options to accept both lineColor & background color
	//gc.Clear()
	center := float64(imgSize) / 2.0

	gc.SetColor(opts.LineColor)
	gc.SetLineWidth(float64(opts.LineWidth))

	for cell := range g.EachCell() {

		theta := 2.0 * math.Pi / float64(g.Rows())
		inner_radius := float64(cell.row * opts.CellSize)
		outer_radius := float64((cell.row + 1) * opts.CellSize)
		theta_ccw := float64(cell.column) * theta
		theta_cw := float64(cell.column+1) * theta

		ax := center + (inner_radius * math.Cos(theta_ccw))
		ay := center + (inner_radius * math.Sin(theta_ccw))
		cx := center + (inner_radius * math.Cos(theta_cw))
		cy := center + (inner_radius * math.Sin(theta_cw))
		dx := center + (outer_radius * math.Cos(theta_cw))
		dy := center + (outer_radius * math.Sin(theta_cw))

		// why no bx, by??
		gc.DrawLine(ax, ay, cx, cy)
		gc.DrawLine(cx, cy, dx, dy)
		gc.Stroke()
	}

	radius := float64(g.Rows() * opts.CellSize)

	//gc.DrawArc(center, center, radius, 0.0, 2.0*math.Pi)
	gc.DrawCircle(center, center, radius)
	gc.Stroke()
	if err := gc.SavePNG(filePath); err != nil {
		fmt.Println(err)
	}
}

func ToPNG(filePath string, g Grid, opts *Options) {
	if opts == nil {
		opts = NewOptions()
	}

	size := opts.LineWidth + opts.CellSize
	lineOffset := 0.0
	lineWidth64 := float64(opts.LineWidth)
	if lineWidth64 > 1.0 {
		lineOffset = lineWidth64 / 2.0
	}

	gc := gg.NewContext(g.Columns()*size+opts.LineWidth, g.Rows()*size+opts.LineWidth)
	//gc.SetColor(color.White)
	//gc.Clear()

	var x1, x2, y1, y2 float64
	for cell := range g.EachCell() {
		x1 = lineOffset + float64(cell.column*size)
		y1 = lineOffset + float64(cell.row*size)
		x2 = lineOffset + float64((cell.column+1)*size)
		y2 = lineOffset + float64((cell.row+1)*size)

		if opts.CellColor != nil {
			gc.SetColor(opts.CellColor(cell))
		} else {
			gc.SetColor(color.White)
		}

		gc.DrawRectangle(x1, y1, x2-x1, y2-y1)
		gc.Fill()

		gc.SetLineWidth(lineWidth64)
		gc.SetLineCapButt()
		gc.SetLineJoinBevel()
		gc.SetColor(opts.LineColor)
		line(gc, cell.north == nil, x1, y1, x2, y1)
		line(gc, cell.west == nil, x1, y1, x1, y2)
		line(gc, !cell.IsLinked(cell.east), x2, y1, x2, y2)
		line(gc, !cell.IsLinked(cell.south), x1, y2, x2, y2)
		gc.Stroke()
	}

	if err := gc.SavePNG(filePath); err != nil {
		fmt.Println(err)
	}
}

func ColorDistancePNG(filename string, g Grid) {
	ToPNG(filename, g, NewOptions(
		LineColor(color.RGBA{0xff, 0x99, 0x00, 0xff}),
		LineWidth(4),
		CellSize(16),
		CellColor(DistanceColorizer(g)),
	))
}

func SolutionPNG(filename string, g Grid) {
	ToPNG(filename, g, NewOptions(
		LineColor(color.Black),
		LineWidth(4),
		CellSize(16),
		CellColor(SolutionColorizer(g)),
	))
}

// PlainPNG just draws the maze, white background with black lines.
func PlainPNG(filename string, g Grid) {
	ToPNG(filename, g, NewOptions())
}

func DistanceColorizer(g Grid) ColorFunc {
	root := g.RandomCell()
	distances := root.Distances()
	maxCell, maxDistance := distances.Max()
	path := distances.PathTo(maxCell)

	return func(c *Cell) color.Color {
		if maxCell.String() == c.String() {
			// start
			return color.RGBA{0x00, 0xcc, 0x33, 0xff}
		} else if root.String() == c.String() {
			// stop
			return color.RGBA{0xee, 0x00, 0x00, 0xff}
		} else if path.Distance(c) != -1 {
			// path from stop to start
			return color.RGBA{0xff, 0xff, 0x66, 0xff}
		} else {
			// rest of maze - distance
			dst := distances.Distance(c)
			intensity := uint8(255 * (maxDistance - dst) / maxDistance)
			return color.RGBA{0x00, intensity / 3, intensity, 255}
		}
	}
}

func SolutionColorizer(g Grid) ColorFunc {
	root := g.RandomCell()
	distances := root.Distances()
	maxCell, _ := distances.Max()
	path := distances.PathTo(maxCell)

	return func(c *Cell) color.Color {
		if maxCell.String() == c.String() {
			// start
			return color.RGBA{0x00, 0x99, 0x33, 0xff}
		} else if root.String() == c.String() {
			// stop
			return color.RGBA{0xee, 0x00, 0x00, 0xff}
		} else if path.Distance(c) != -1 {
			// solution
			return color.RGBA{0xbb, 0xdd, 0x66, 0xff}
		} else {
			// rest of maze - distance
			return color.RGBA{0xff, 0xff, 0xff, 0xff}
		}
	}
}
