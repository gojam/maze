package maze

import (
	"bufio"
	"fmt"
	"image"
	_ "image/png"
	"log"
	"math/rand"
	"os"
	"strings"
)

type Mask struct {
	rows, columns int
	bits          [][]bool
}

// NewMask creates a new bitmask which is used to create non-rectangular grids.
func NewMask(rows, columns int) *Mask {
	rowList := make([][]bool, rows)
	for r := 0; r < rows; r++ {
		colList := make([]bool, columns)
		for c := 0; c < columns; c++ {
			colList[c] = true
		}
		rowList[r] = colList
	}

	return &Mask{rows, columns, rowList}
}

func NewMaskFromTextFile(path string) *Mask {
	file, err := os.Open(path)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	rows := 0
	columns := 0
	scanner := bufio.NewScanner(file)
	var sfmt string

	var lines []string
	for scanner.Scan() {
		line := strings.TrimSpace(scanner.Text())
		if len(line) > 0 {
			if rows == 0 {
				columns = len(line)

				// this becomes something like "%-20s", a sprintf format string
				// to ensure all lines are at least (columns) wide
				sfmt = fmt.Sprintf("%%-%ds", columns)
			}

			rows++
			lines = append(lines, fmt.Sprintf(sfmt, line))
		}
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	rowList := make([][]bool, rows)
	for r := 0; r < rows; r++ {
		colList := make([]bool, columns)
		for c := 0; c < columns; c++ {
			ch := string([]rune(lines[r])[c])
			if ch == "X" {
				colList[c] = false
			} else {
				colList[c] = true
			}
		}

		rowList[r] = colList
	}

	return &Mask{rows, columns, rowList}
}

func NewMaskFromPNG(path string) *Mask {
	file, err := os.Open(path)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	img, _, err := image.Decode(file)
	if err != nil {
		log.Fatal(err)
	}

	bounds := img.Bounds()
	rows := bounds.Max.Y
	columns := bounds.Max.X

	rowList := make([][]bool, rows)
	for r := 0; r < rows; r++ {
		colList := make([]bool, columns)
		for c := 0; c < columns; c++ {

			red, green, blue, _ := img.At(c, r).RGBA()
			// colors are multiplied by alpha, so (0 <  color < 65535)
			// if color is dark, turn it off
			if red < 10000 && green < 10000 && blue < 10000 {
				colList[c] = false
			} else {
				colList[c] = true
			}
		}

		rowList[r] = colList
	}

	return &Mask{rows, columns, rowList}
}

// Get retrieves the enabled value of the specified point.  If the point
// does not exist, Get returns false.
func (m *Mask) Get(row, column int) bool {
	if row < 0 || row >= m.rows {
		return false
	}

	if column < 0 || column >= m.columns {
		return false
	}

	return m.bits[row][column]
}

// Set sets the enabled bit of the specified point in the mask
func (m *Mask) Set(row, column int, enabled bool) {
	if row >= 0 && row < m.rows && column >= 0 && column < m.columns {
		m.bits[row][column] = enabled
	}
}

func (m *Mask) Count() int {
	count := 0
	for r := 0; r < m.rows; r++ {
		for c := 0; c < m.rows; c++ {
			if m.bits[r][c] {
				count++
			}
		}
	}

	return count
}

// RandomLocation returns a random enabled position (row, col).
func (m *Mask) RandomLocation() (int, int) {
	for {
		r := rand.Intn(m.rows)
		c := rand.Intn(m.columns)

		if m.bits[r][c] {
			return r, c
		}
	}
}
