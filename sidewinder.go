package maze

import (
	"math/rand"
)

func Sidewinder(g Grid) {
	for row := range g.EachRow() {
		var run []*Cell

		for _, cell := range row {
			run = append(run, cell)

			atEasternBoundary := (cell.east == nil)
			atNorthernBoundary := (cell.north == nil)

			shouldCloseOut := atEasternBoundary || (!atNorthernBoundary && rand.Intn(2) == 0)

			if shouldCloseOut {
				member := sample(run)
				if member != nil {
					member.Link(member.north, true)
				}
				run = run[:0] // clear slice
			} else {
				cell.Link(cell.east, true)
			}
		}
	}
}
