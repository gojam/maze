package maze

import (
	"fmt"
	"math/rand"
)

// CompassPoint are bit fields for a compass, north, south, east and west.
type CompassPoint int

const (
	NORTH CompassPoint = 1 << iota // 1 << 0 which is 00000001
	SOUTH                          // 1 << 1 which is 00000010
	EAST                           // 1 << 2 which is 00000100
	WEST                           // 1 << 3 which is 00001000
)

// linkval is the internal struct used in Cell.
type linkval struct {
	cell *Cell
	bidi bool
}

// Cell is a single value in a maze or Grid.
type Cell struct {
	row, column              int
	north, south, east, west *Cell
	links                    map[string]linkval
}

// newCell creates a new cell
func newCell(row int, column int) *Cell {
	return &Cell{row: row, column: column, links: make(map[string]linkval)}
}

func (c *Cell) Row() int {
	return c.row
}

func (c *Cell) Column() int {
	return c.column
}

func (c *Cell) Link(cell *Cell, bidi bool) {
	if cell != nil {
		c.links[cell.String()] = linkval{cell, true}
		if bidi {
			cell.Link(c, false)
		}
	}
}

func (c *Cell) Unlink(cell *Cell, bidi bool) {
	delete(c.links, cell.String())
	if bidi {
		cell.Unlink(c, false)
	}
}

func (c *Cell) Links() []*Cell {
	var keys []*Cell
	for _, val := range c.links {
		keys = append(keys, val.cell)
	}

	return keys
}

func (c *Cell) IsLinked(cell *Cell) bool {
	if cell == nil {
		return false
	}
	_, ok := c.links[cell.String()]
	return ok
}

func (c *Cell) String() string {
	return fmt.Sprintf("(%d,%d)", c.row, c.column)
}

// Neighbors return all of the cells neighbors (from the north/south/east/west).
func (c *Cell) Neighbors() []*Cell {
	var neighbors []*Cell
	if c.north != nil {
		neighbors = append(neighbors, c.north)
	}
	if c.south != nil {
		neighbors = append(neighbors, c.south)
	}
	if c.east != nil {
		neighbors = append(neighbors, c.east)
	}
	if c.west != nil {
		neighbors = append(neighbors, c.west)
	}

	return neighbors
}

// RandomNeighbor returns a random neighor
func (c *Cell) RandomNeighbor() *Cell {
	n := c.Neighbors()
	return n[rand.Intn(len(n))]
}

// WallMask returns a bitmask representing the walls, setting NORTH if there's
// a wall to the north, etc, for NORTH, SOUTH, EAST, WEST
func (c *Cell) WallMask() CompassPoint {
	var mask CompassPoint = 0

	if !c.IsLinked(c.north) {
		mask |= NORTH
	}
	if !c.IsLinked(c.south) {
		mask |= SOUTH
	}
	if !c.IsLinked(c.east) {
		mask |= EAST
	}
	if !c.IsLinked(c.west) {
		mask |= WEST
	}

	return mask
}

func (c *Cell) Distances() *distances {
	d := newDistances(c)
	var frontier []*Cell
	frontier = append(frontier, c)
	for len(frontier) > 0 {
		var new_frontier []*Cell
		for _, cell := range frontier {
			for _, linked := range cell.Links() {
				if d.Distance(linked) == -1 {
					d.setDistance(linked, d.Distance(cell)+1)
					new_frontier = append(new_frontier, linked)
				}
			}
		}

		frontier = new_frontier
	}

	return d
}
