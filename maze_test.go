package maze

import (
	"fmt"
	"testing"
)

func createGrid() *SimpleGrid {
	return NewGrid(32, 32)
}

func TestAldousBorder(t *testing.T) {
	g := createGrid()
	AldousBroder(g)
	PlainPNG("./test/aldous.png", g)
}

func TestBinaryTree(t *testing.T) {
	g := createGrid()
	BinaryTree(g)
	PlainPNG("./test/binary.png", g)
}

func TestHuntAndKill(t *testing.T) {
	g := createGrid()
	HuntAndKill(g)
	ColorDistancePNG("./test/hunt.png", g)
}

func TestRecursiveBacktracker(t *testing.T) {
	g := createGrid()
	RecursiveBacktracker(g)
	ColorDistancePNG("./test/resursive.png", g)
}

func TestSidewinder(t *testing.T) {
	g := createGrid()
	Sidewinder(g)
	PlainPNG("./test/sidewinder.png", g)
}

func TestWilsons(t *testing.T) {
	g := createGrid()
	Wilsons(g)
	ColorDistancePNG("./test/wilsons.png", g)
}

func TestTextMask(t *testing.T) {
	mask := NewMaskFromTextFile("./mask.txt")
	grid := NewMaskedGrid(mask)
	RecursiveBacktracker(grid)
	fmt.Println(grid)
}
