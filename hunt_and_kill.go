package maze

func HuntAndKill(g Grid) {
	current := g.RandomCell()

	for current != nil {
		unvisitedNeighbors := unlinkedCells(current.Neighbors())
		if len(unvisitedNeighbors) > 0 {
			neighbor := sample(unvisitedNeighbors)
			current.Link(neighbor, true)
			current = neighbor
		} else {
			current = nil

			for cell := range g.EachCell() {
				visitedNeighbors := linkedCells(cell.Neighbors())
				if len(cell.Links()) == 0 && len(visitedNeighbors) > 0 {
					current = cell
					neighbor := sample(visitedNeighbors)
					current.Link(neighbor, true)
					break
				}
			}
		}
	}
}
